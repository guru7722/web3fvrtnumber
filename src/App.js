import { ethers, providers } from "ethers";
import "./style/main.css";
import { useEffect, useState } from "react";
import { abi, ContractAddress } from "./constense";

function App() {
  //states
  const [isConnected, setconnect] = useState(null);
  const [provider, setProvider] = useState(null);
  const [signer, setsigner] = useState(null);
  const [contract, setContract] = useState(null);
  const [fvrtnumber, setfavrtnumber] = useState(null);
  const [updateNewVal, setVal] = useState("");
  const [contractAddress, setaddress] = useState("");

  useEffect(() => {
    if (isConnected) {
      console.log(isConnected);
    }
  });

  //setting provider

  async function connectToMetaMask() {
    if (window.ethereum) {
      console.log("ugot meta mask"); // const provider = new ethers.providers.Web3Provider(window.ethereum);
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      setProvider(provider);
      const permmisions = await provider.send("eth_requestAccounts"); //requsting for permmisions from metamask
      if (permmisions) {
        setconnect(true);
      }
      const signer = provider.getSigner();
      setsigner(signer);
      const contract = new ethers.Contract(ContractAddress, abi, signer);
      setContract(contract);
    } else {
      console.log("install meta mask");
    }
  }

  //getting fvrt number from contract
  async function GetFvrtNumber() {
    if (isConnected) {
      const fvrtNumber = await contract.retrieve();
      setfavrtnumber(fvrtNumber.toString());
    } else {
      console.log("make sure u are connected to wallet....");
    }
  }
  //updating fvrt number
  async function UpdateNumber() {
    const val = Number(updateNewVal);
    await contract.store(val);
  }
  /*--------------------------------------------------FUNCTION BTN HANDLES-----------------------------------------------------------------------------------------------------------------------*/
  //connecting BTN handling Function
  function connectME() {
    console.log("clicked");
    connectToMetaMask()
      .then((res) => console.log(res))
      .catch((e) => console.error(e));
  }
  //get favrt Number BTN handle
  function getnumber() {
    GetFvrtNumber()
      .then(() => console.log())
      .catch((e) => console.error(e));
  }
  // updating Number
  function updateNumber() {
    UpdateNumber()
      .then(() => console.log("dvrt number is updated"))
      .catch((e) => console.error(e));
  }

  //from handling
  function handleData(e) {
    try {
      e.preventDefault();
      if (isConnected) {
        updateNumber();
      } else {
        console.log("cannot update value due to disconnected wallet");
      }
    } catch (e) {
      console.error(e);
    }
  }
  //get contract address
  function getaddress() {
    // const contract = new ethers.Contract(ContractAddress, abi, signer);
    if (isConnected) {
      setaddress(contract.address);
    } else {
      console.log("make sure you are connected to wallet ....");
    }
  }
  return (
    <div className="App">
      <div className="output-div">
        <p>
          <strong>FavrtNumber :</strong>
          {isConnected ? <em>{fvrtnumber}</em> : <>connect to wallet first</>}
        </p>
        <p>
          <strong>ContractAddress :</strong>
          {isConnected ? (
            <em>{contractAddress}</em>
          ) : (
            <>connect to wallet first</>
          )}
        </p>
      </div>
      <button onClick={connectME}>connect</button>
      <button onClick={getnumber}>getfavrtNumber</button>
      <form onSubmit={handleData}>
        <label>
          <input
            type="number"
            value={updateNewVal}
            required
            onChange={(e) => setVal(e.target.value)}
          />
        </label>
        <button type="submit">updateFvrtNumber</button>
      </form>
      <button onClick={getaddress}>GetContractAdress</button>
    </div>
  );
}

export default App;
